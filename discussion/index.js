// alert('hi')

// Common examples of an array
// Arrays and Indexes
	/*
	    - Arrays are used to store multiple related values in a single variable
	    - They are declared using square brackets ([]) also known as "Array Literals"
	    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
	    - Arrays also provide access to a number of functions/methods that help in achieving this
	    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
	    - Majority of methods are used to manipulate information stored within the same object
	    - Arrays are also objects which is another data type
	    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
	    - Syntax
	        let/const arrayName = [elementA, elementB, ElementC...]
	*/

let marvelHeroes = ['Iron Man', 'Captain America', 'Thor', 'Hulk', 'Black Widow', 'Hawkeye', 'Spiderman', 'Daredevil']
console.log(marvelHeroes);

// Another way of writing an array
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake react'
]
// console.log(myTasks);

// Re-assigning array values
console.log('Array before reassignment');
console.log(myTasks);

// arrayName[index] = 'updated value'
myTasks[0] = 'dream coding'
console.log('Array after reassignment');
console.log(myTasks);

console.log(marvelHeroes[20]);
// result- undefined

// Reading from Arrays
	/*
		Accessing array elements is one of the more common tasks that we do with an array
		This can be done through the use of array indexes
		Each element in an array is associated with it's own index/number
		In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
		The reason an array starts with 0 is due to how the language is designed

	*/
/*
	Syntax: 
		arrayName[index]
*/

console.log(myTasks[2]);

// Getting the length of an array

console.log(marvelHeroes.length);

if(marvelHeroes.length > 5){
	console.log('We have too many heroes, please reach Thanos.')
}

// Accessing the last element of an array

let lastElementIndex = marvelHeroes.length - 1;
console.log(marvelHeroes[lastElementIndex]);

// Array Methods

	// Mutator Methods
		/*
		    - Mutator methods are functions that "mutate" or change an array after they're created
		    - These methods manipulate the original array performing various tasks such as adding and removing elements
		*/


let fruits = ['Apple', 'Cherry', 'Mango', 'Lime']
// push()	
	// Adds an element in the end of an array AND returns the array's length
	// Syntax: arrayName.push()

console.log('current array:')
console.log(fruits);

let fruitsLength = fruits.push('Kiwi')
console.log(fruitsLength);

console.log('Mutated array from push:');
console.log(fruits);



// pop()
	// Removes the last element in an array AND returns the removed element
	// Syntax: arrayName.pop()

let removedFruit = fruits.pop();
console.log(removedFruit);

console.log('Mutated array from pop method:')
console.log(fruits);




// unshift()
	// adding elements at the beginning of an array
	// Syntax: arrayName.unshift('elementA')
	// 		arrayName.unshift('elementA, elementB')

fruits.unshift('Lime', 'Lemon');
console.log('Mutated array from unshift method:')
console.log(fruits);




// shift()
	// Removes element at the beginning of an array and returns the removed element
	// Syntax: arrayName.shift()

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);



	
// splice()
	// Simultaneously removes elements from a specified index number and adds elements
	// Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

fruits.splice(1, 2, 'Dragon Fruit', 'Watermelon');
console.log('Mutated array from splice method:');
console.log(fruits);




// sort()
	// Rearranges the array elements in alphanumeric order
	// Syntax: arrayName.sort()
	
fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);





// reverse()
	// Reverses the order of array elements
	// Syntax: arrayName.reverse()

fruits.reverse();
console.log('Mutated array from reverse method:');
console.log(fruits);




// Non-Mutator Methods
	/*
	    - Non-Mutator methods are functions that do not modify or change an array after they're created
	    - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'KR', 'FR', 'DE']

// indexOf()
	// Returns the index number of the first matching element found in an array
	// If no match was found, the result will be -1.
	// The search process will be done from first element proceeding to the last element
	// Syntax: arrayName.indexOf(searchValue)
	//  	   arrayName.indexOf(searchValue, fromIndex)

let firstIndex = countries.indexOf('PH');
console.log('Result from indexOf method:' + firstIndex);
// result= 1

let invalidCountry = countries.indexOf('BR');
console.log('Result from indexOf method:' + invalidCountry);
// result= -1





// slice()
	// Portions/slices elements from an array AND returns a new array
	// Syntax: arrayName.slice(startingIndex)
	//         arrayName.slice(startingIndex, endingIndex)

// Slice off elements from a specified index
let slicedArrayA = countries.slice(2);
console.log('Result from slice method:');
console.log(slicedArrayA);
console.log(countries);


let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method:');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method:');
console.log(slicedArrayC);



// toString()
	// Returns an array as a string separated by commas
	// Syntax: arrayName.toString()

let stringArray = countries.toString();
console.log('Result from toString:');
console.log(stringArray);




// concat()
	// Combines two arrays and returns the combined result
	// Syntax: arrayA.concat(arrayB)
	// 		   arrayA.concat(elementA)

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'exhale bootstrap'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks);

// Combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method:');
console.log(combinedTasks);




// join()
	// Returns an array as a string separated by specified separator string
	// Syntax:
		// arrayName.join('separatorString')
			// separatorString-(), (''), ('-')

let users = ['Tanjiro', 'Nezuko', 'Zenitsu'];

console.log(users.join());
console.log(users.join(''));
console.log(users.join('-'));




// Iteration Methods
	/*
	  Iteration methods are loops designed to perform repetitive tasks on arrays
	   Useful for manipulating array data resulting in complex tasks
	*/



// forEach
	/*
		Similar to a for loop that iterates on each array element
		Variable names for arrays are normally written in the plural form of the data stored in an array
		It's common practice to use the singular form of the array content for parameter names used in array loops
		Array iteration methods normally work with a function supplied as an argument

		Syntax
	        arrayName.forEach(function(indivElement) {
	            statement
	        })
	
	*/

let filteredTasks = [];

allTasks.forEach(function(task){
	// console.log(task);

	if(task.length > 10){
		filteredTasks.push(task);
	}
});

console.log('Result from filtered tasks:')
console.log(filteredTasks);



// map()
	/*
		Iterates on each element AND returns new array with different values depending on the result of the function's operation
		This is useful for performing tasks where mutating/changing the elements are required
		Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
		
		Syntax
        let/const resultArray = arrayName.map(function(indivElement))

	*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	return number * number
})
console.log('Result of map method:');
console.log(numberMap);




// every()

	/*
		Checks if all elements in an array meet the given condition
		This is useful for validating data stored in arrays especially when dealing with large amounts of data
		Returns a true value if all elements meet the condition and false if otherwise

		Syntax
	        let/const resultArray = arrayName.every(function(indivElement) {
	            return expression/condition;
	        })

	*/

let allValid = numbers.every(function(number){
	return (number < 3)
})
console.log('Result of every method:');
console.log(allValid);




// some()

	/*
		Checks if at least one element in the array meets the given condition
		Returns a true value if at least one element meets the condition and false if otherwise

		Syntax
        let/const resultArray = arrayName.some(function(indivElement) {
            return expression/condition;
        })

	*/
let someValid = numbers.some(function(number){
	return (number < 2)
});
console.log('Result from some method:')
console.log(someValid);





// filter()

	/*
		Returns new array that contains elements which meets the given condition
		Returns an empty array if no elements were found
		Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods

		Syntax
        let/const resultArray = arrayName.filter(function(indivElement) {
            return expression/condition;
        })

	*/


let filterValid = numbers.filter(function(number){
	return (number < 3)
});
console.log('Result from filter method:');
console.log(filterValid);

// filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){

	if(number < 3){
		filteredNumbers.push(number);

	};

});
console.log('Filtered through forEach:')
console.log(filteredNumbers);



// includes
	/*
		Methods can be "chained" using them one after another
		The result of the first method is used on the second method until all "chained" methods have been resolved

	*/
let cars = ['Hyundai', 'Ford', 'Toyota', 'BMW'];

let filteredBrands = cars.filter(function(brand){
	return brand.toLowerCase().includes('a')
})
console.log(filteredBrands);




// reduce
	/* 
	   Evaluates elements from left to right and returns/reduces the array into a single value
	    Syntax
	        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
	            return expression/operation
	        })
	    The "accumulator" parameter in the function stores the result for every iteration of the loop
	    The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
	    How the "reduce" method works
	        1. The first/result element in the array is stored in the "accumulator" parameter
	        2. The second/next element in the array is stored in the "currentValue" parameter
	        3. An operation is performed on the two elements
	        4. The loop repeats step 1-3 until all elements have been worked on
	*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration:' + ++iteration);
	console.log('accumulator:' + x);
	console.log('currentValue:' + y);

	return x + y
})
console.log('Result from reduce method:' + reducedArray);

let sentence = ['The', 'brain', 'is', 'loading']

let reducedSentence = sentence.reduce(function(x, y){
	return x + " " + y
});
console.log('Result from reduce method:' + reducedSentence)

// Multidimensional Arrays
	/*
	    - Multidimensional arrays are useful for storing complex data structures
	    - A practical application of this is to help visualize/create real world objects
	
	*/


let chessboard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],

]
console.log(chessboard);

console.log(chessboard[1][4]);

console.log('Pawn moves to:' + chessboard[1][5]);
